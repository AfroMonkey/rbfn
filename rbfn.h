#ifndef RBFN_H
#define RBFN_H

#include <cmath>

#define POINT std::vector<double>
#define RELATION std::pair<POINT, size_t>
#define CLUSTER std::pair<POINT, double>

#include <vector>

class RBFN {
private:
    std::vector<RELATION> relations;
    std::vector<CLUSTER> clusters;
    bool centered;

    void assign_clusters();

public:
    RBFN(std::vector<POINT> points, const size_t number_of_clusters);
    std::vector<CLUSTER> update_clusters();
    bool isCentered() { return centered; }
    std::vector<CLUSTER> getClusters() { return clusters; }

    static double gaussian(double r, double e) {
        return exp(-(r * r) / (e * e));
    }
    static double getGaussian(CLUSTER &cluster, double x);
};

#endif // RBFN_H
