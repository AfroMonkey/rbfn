#include "mainwindow.h"

#include "ui_mainwindow.h"

#define X_AXIS 10
#define Y_AXIS 10
#define CLOCK_FREQUENCY 100
#define GAUSS false
#define GAUSS_STEP X_AXIS / 100.0

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
  , rbfn(nullptr) {
    ui->setupUi(this);

    prepare_plot();

    connect(ui->mainPlot, &QCustomPlot::mousePress, this, &MainWindow::main_plot_clicked);
    connect(&clock, &QTimer::timeout, this, &MainWindow::tick);
}

MainWindow::~MainWindow() {
    delete ui;
    if (rbfn) {
        delete rbfn;
    }
}

void MainWindow::prepare_plot() {
    ui->mainPlot->xAxis->setRange(-X_AXIS, X_AXIS);
    ui->mainPlot->yAxis->setRange(-Y_AXIS, Y_AXIS);

    // Points
    ui->mainPlot->addGraph();
    ui->mainPlot->graph(0)->setLineStyle(QCPGraph::lsNone);
    ui->mainPlot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 5));
    // Clusters
    ui->mainPlot->addGraph();
    ui->mainPlot->graph(1)->setLineStyle(QCPGraph::lsNone);
    ui->mainPlot->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssStar, 5));

    ui->mainPlot->replot();
}

void MainWindow::main_plot_clicked(QMouseEvent *e) {
    QPointF point = QPointF(ui->mainPlot->xAxis->pixelToCoord(e->x()), ui->mainPlot->yAxis->pixelToCoord(e->y()));

    points.push_back({point.x(), point.y()});
    ui->mainPlot->graph(0)->addData(point.x(), point.y());
    ui->mainPlot->replot();
}

void MainWindow::tick() {
    clusters = rbfn->update_clusters();
    plot_clusters();
    if (rbfn->isCentered()) {
        clock.stop();
        QMessageBox(QMessageBox::Information, "Done", "Centered").exec();
        plot_clusters(true);
    }
}

void MainWindow::plot_clusters(bool radious) {
    ui->mainPlot->graph(1)->setData(QVector<double>(), QVector<double>());
    for (size_t c = 0; c < clusters.size(); ++c) {
        auto &cluster = clusters[c];
        ui->mainPlot->graph(1)->addData(cluster.first.at(0), cluster.first.at(1));
        circles[c]->topLeft->setCoords(cluster.first.at(0) - cluster.second, cluster.first.at(1) + cluster.second);
        circles[c]->bottomRight->setCoords(cluster.first.at(0) + cluster.second, cluster.first.at(1) - cluster.second);
        circles[c]->setVisible(radious and not GAUSS);
        if (GAUSS and radious) {
            auto range = ui->mainPlot->xAxis->range();
            for (auto x = range.lower; x < range.upper; x += GAUSS_STEP) {
                ui->mainPlot->graph(2 + static_cast<int>(c))->addData(x, RBFN::getGaussian(cluster, x));
            }
        }
    }
    ui->mainPlot->replot();
}

void MainWindow::on_initButton_clicked() {
    on_clearButton_clicked(false);
    rbfn = new RBFN(points, static_cast<size_t>(ui->numberOfClustersSpinBox->value()));
    clusters = rbfn->getClusters();
    circles = vector<QCPItemEllipse *>(static_cast<size_t>(ui->numberOfClustersSpinBox->value()));
    for (auto &circle : circles) {
        circle = new QCPItemEllipse(ui->mainPlot);
        // Gauss
        ui->mainPlot->addGraph();
    }
    plot_clusters();
}

void MainWindow::on_processButton_clicked() {
    clock.start(CLOCK_FREQUENCY);
}

void MainWindow::on_clearButton_clicked(bool clear_data) {
    if (clear_data) {
        points.clear();
        ui->mainPlot->graph(0)->setData({});
        ui->mainPlot->graph(1)->setData({});
    }
    while (ui->mainPlot->graphCount() > 2) {
        ui->mainPlot->removeGraph(2);
    }
    if (rbfn) {
        delete rbfn;
        rbfn = nullptr;
    }
    while (circles.size()) {
        ui->mainPlot->removeItem(circles.back());
        circles.pop_back();
    }
    ui->mainPlot->replot();
}
