#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "qcustomplot.h"
#include "rbfn.h"

#include <QMainWindow>
#include <QTimer>

using namespace std;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void main_plot_clicked(QMouseEvent *e);
    void tick();
    void on_initButton_clicked();
    void on_processButton_clicked();
    void on_clearButton_clicked(bool clear_data = false);

private:
    Ui::MainWindow *ui;
    QTimer clock;
    vector<POINT> points;
    vector<CLUSTER> clusters;
    RBFN *rbfn;

    vector<QCPItemEllipse *> circles;

    void prepare_plot();
    void plot_clusters(bool radious = false);
};

#endif // MAINWINDOW_H
