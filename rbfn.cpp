#include "rbfn.h"

#include <algorithm>
#include <cmath>

RBFN::RBFN(std::vector<POINT> points, const size_t number_of_clusters)
  : centered(false) {
    for (auto point : points) {
        relations.push_back({point, 0});
    }
    std::random_shuffle(points.begin(), points.end());
    for (size_t p = 0; p < number_of_clusters; ++p) {
        clusters.push_back({points[p], 0});
    }
}

std::vector<CLUSTER> RBFN::update_clusters() {
    if (not centered) {
        // Clustering
        assign_clusters();

        // Centering
        for (size_t c = 0; c < clusters.size(); ++c) {
            auto cluster = &clusters[c];
            cluster->second = -1;
            size_t number_of_neighboors = 0;
            for (size_t d = 0; d < cluster->first.size(); ++d) {
                cluster->first.at(d) = 0;
            }
            for (size_t p = 0; p < relations.size(); ++p) {
                if (relations[p].second == c) {
                    for (size_t d = 0; d < cluster->first.size(); ++d) {
                        cluster->first.at(d) += relations[p].first[d];
                    }
                    ++number_of_neighboors;
                }
            }
            if (number_of_neighboors) {
                for (size_t d = 0; d < cluster->first.size(); ++d) {
                    cluster->first.at(d) /= number_of_neighboors;
                }
            }
        }
        assign_clusters();
    }
    return clusters;
}

void RBFN::assign_clusters() {
    centered = true;
    for (size_t p = 0; p < relations.size(); ++p) {
        auto relation = &relations[p];
        std::vector<double> distance_to_clusters;
        for (auto cluster : clusters) {
            double distance = 0;
            for (size_t d = 0; d < cluster.first.size(); ++d) {
                double diff = cluster.first[d] - relation->first[d];
                distance += diff * diff;
            }
            distance = sqrt(distance);
            distance_to_clusters.push_back(distance);
        }
        auto prev = relation->second;
        auto min_distance_index = std::min_element(distance_to_clusters.begin(), distance_to_clusters.end());
        auto min_distance = *min_distance_index;
        relation->second = static_cast<size_t>(min_distance_index - distance_to_clusters.begin());
        if (clusters[relation->second].second < min_distance) {
            clusters[relation->second].second = min_distance;
        }
        if (prev != relation->second) {
            centered = false;
        }
    }
}

double RBFN::getGaussian(CLUSTER &cluster, double x) {
    // TODO check final product
    return (gaussian(x - cluster.first.at(0), cluster.second)) + cluster.first.at(1);
}
